﻿
namespace LensflareGameInterface.Input.Keys
{
    public enum GamepadKey
    {
        A,
        B,
        X,
        Y,
        RightShoulder,
        LeftShoulder,
        LeftTrigger,
        RightTrigger,
        DPadUp,
        DPadDown,
        DPadLeft,
        DPadRight,
        Menu,
        View,
        LeftThumbstickButton,
        RightThumbstickButton,
        LeftThumbstickUp,
        LeftThumbstickDown,
        LeftThumbstickRight,
        LeftThumbstickLeft,
        RightThumbstickUp,
        RightThumbstickDown,
        RightThumbstickRight,
        RightThumbstickLeft,
        Other, //fallback key
    }
}
