﻿
namespace LensflareGameInterface.Input.Keys
{
    public enum MouseKey
    {
        LeftButton,
        RightButton,
        MiddleButton,
        XButton1,
        XButton2,
        Other, //fallback key
    }
}
