﻿using System.Numerics;
using LensflareGameInterface.Input.Keys;

namespace LensflareGameInterface.Input
{
    //TODO: Touch
    //TODO: Mouse X,Y
    //TODO: Mouse Delta X,Y

    public interface IInput
    {
        void Update();

        bool KeyboardDown(KeyboardKey key);
        bool KeyboardPressed(KeyboardKey key);
        bool KeyboardReleased(KeyboardKey key);

        bool GamePadDown(GamepadKey key);
        bool GamePadPressed(GamepadKey key);
        bool GamePadReleased(GamepadKey key);

        bool MouseDown(MouseKey key);
        bool MousePressed(MouseKey key);
        bool MouseReleased(MouseKey key);

        int MouseWheelDelta();

        Vector2 MousePosition();
        Vector2 MousePositionDelta();
    }
}
