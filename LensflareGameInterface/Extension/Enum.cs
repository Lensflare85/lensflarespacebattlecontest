﻿using System;

namespace LensflareGameInterface {
    public static class EnumExtension {
        public static int GetLength<T>() {
            return Enum.GetNames(typeof(T)).Length;
        }
    }
}
