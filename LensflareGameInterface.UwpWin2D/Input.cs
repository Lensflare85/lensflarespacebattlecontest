﻿using System;
using System.Numerics;
using LensflareGameInterface.Input;
using LensflareGameInterface.Input.Keys;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.Xaml;

namespace LensflareGameInterface.UwpWin2D
{
    public sealed class Input : IInput {

        bool[] mouseKeyDownPrev = new bool[EnumExtension.GetLength<MouseKey>()];
        bool[] mouseKeyDown = new bool[EnumExtension.GetLength<MouseKey>()];

        bool[] gamepadKeyDownPrev = new bool[EnumExtension.GetLength<GamepadKey>()];
        bool[] gamepadKeyDown = new bool[EnumExtension.GetLength<GamepadKey>()];

        bool[] keyboardKeyDownPrev = new bool[EnumExtension.GetLength<KeyboardKey>()];
        bool[] keyboardKeyDown = new bool[EnumExtension.GetLength<KeyboardKey>()];

        int mouseWheelDeltaSum = 0;
        int mouseWheelDeltaSumCurrent = 0;

        Vector2 mousePositionPrev;
        Vector2 mousePosition;

        public Input() {
            Window.Current.CoreWindow.PointerWheelChanged += (sender, args) => {
                var intermediatePoints = args.GetIntermediatePoints();
                var currentPoint = args.CurrentPoint;
                mouseWheelDeltaSum += currentPoint.Properties.MouseWheelDelta;
            };
        }

        public void Update() {
            mouseKeyDownPrev = (bool[])mouseKeyDown.Clone();
            gamepadKeyDownPrev = (bool[])gamepadKeyDown.Clone();
            keyboardKeyDownPrev = (bool[])keyboardKeyDown.Clone();

            foreach (var x in Enum.GetValues(typeof(VirtualKey))) {
                var virtualKey = (VirtualKey)x;
                bool virtualKeyDown = Window.Current.CoreWindow.GetAsyncKeyState(virtualKey) != CoreVirtualKeyStates.None;

                var keyboardKey = MapKeyboardKey(virtualKey);
                if (keyboardKey != KeyboardKey.Other) {
                    keyboardKeyDown[(int)keyboardKey] = virtualKeyDown;
                } else {
                    var gamepadKey = MapGamepadKey(virtualKey);
                    if (gamepadKey != GamepadKey.Other) {
                        gamepadKeyDown[(int)gamepadKey] = virtualKeyDown;
                    } else {
                        var mouseKey = MapMouseKey(virtualKey);
                        if (mouseKey != MouseKey.Other) {
                            mouseKeyDown[(int)mouseKey] = virtualKeyDown;
                        }
                    }
                }
            }

            mouseWheelDeltaSumCurrent = mouseWheelDeltaSum;
            mouseWheelDeltaSum = 0;

            mousePositionPrev = mousePosition;
            mousePosition = MousePosition();
        }

        public bool KeyboardDown(KeyboardKey key) {
            return keyboardKeyDown[(int)key];
        }

        public bool KeyboardPressed(KeyboardKey key) {
            return !keyboardKeyDownPrev[(int)key] && keyboardKeyDown[(int)key];
        }

        public bool KeyboardReleased(KeyboardKey key) {
            return keyboardKeyDownPrev[(int)key] && !keyboardKeyDown[(int)key];
        }


        public bool GamePadDown(GamepadKey key) {
            return gamepadKeyDown[(int)key];
        }

        public bool GamePadPressed(GamepadKey key) {
            return !gamepadKeyDownPrev[(int)key] && gamepadKeyDown[(int)key];
        }

        public bool GamePadReleased(GamepadKey key) {
            return gamepadKeyDownPrev[(int)key] && !gamepadKeyDown[(int)key];
        }


        public bool MouseDown(MouseKey key) {
            return mouseKeyDown[(int)key];
        }

        public bool MousePressed(MouseKey key) {
            return !mouseKeyDownPrev[(int)key] && mouseKeyDown[(int)key];
        }

        public bool MouseReleased(MouseKey key) {
            return mouseKeyDownPrev[(int)key] && !mouseKeyDown[(int)key];
        }

        public int MouseWheelDelta() {
            return mouseWheelDeltaSumCurrent;
        }

        public Vector2 MousePosition() {
            var position = Window.Current.CoreWindow.PointerPosition;
            return new Vector2((float)position.X, (float)position.Y);
        }

        public Vector2 MousePositionDelta() {
            return mousePosition - mousePositionPrev;
        }

        static public MouseKey MapMouseKey(VirtualKey key) {
            switch (key) {
                case VirtualKey.LeftButton: return MouseKey.LeftButton;
                case VirtualKey.RightButton: return MouseKey.RightButton;
                case VirtualKey.MiddleButton: return MouseKey.MiddleButton;
                case VirtualKey.XButton1: return MouseKey.XButton1;
                case VirtualKey.XButton2: return MouseKey.XButton2;
                default: return MouseKey.Other;
            }
        }

        static public GamepadKey MapGamepadKey(VirtualKey key) {
            switch(key) {
                case VirtualKey.GamepadA: return GamepadKey.A;
                case VirtualKey.GamepadB: return GamepadKey.B;
                case VirtualKey.GamepadX: return GamepadKey.X;
                case VirtualKey.GamepadY: return GamepadKey.Y;
                case VirtualKey.GamepadRightShoulder: return GamepadKey.RightShoulder;
                case VirtualKey.GamepadLeftShoulder: return GamepadKey.LeftShoulder;
                case VirtualKey.GamepadLeftTrigger: return GamepadKey.LeftTrigger;
                case VirtualKey.GamepadRightTrigger: return GamepadKey.RightTrigger;
                case VirtualKey.GamepadDPadUp: return GamepadKey.DPadUp;
                case VirtualKey.GamepadDPadDown: return GamepadKey.DPadDown;
                case VirtualKey.GamepadDPadLeft: return GamepadKey.DPadLeft;
                case VirtualKey.GamepadDPadRight: return GamepadKey.DPadRight;
                case VirtualKey.GamepadMenu: return GamepadKey.Menu;
                case VirtualKey.GamepadView: return GamepadKey.View;
                case VirtualKey.GamepadLeftThumbstickButton: return GamepadKey.LeftThumbstickButton;
                case VirtualKey.GamepadRightThumbstickButton: return GamepadKey.RightThumbstickButton;
                case VirtualKey.GamepadLeftThumbstickUp: return GamepadKey.LeftThumbstickUp;
                case VirtualKey.GamepadLeftThumbstickDown: return GamepadKey.LeftThumbstickDown;
                case VirtualKey.GamepadLeftThumbstickRight: return GamepadKey.LeftThumbstickRight;
                case VirtualKey.GamepadLeftThumbstickLeft: return GamepadKey.LeftThumbstickLeft;
                case VirtualKey.GamepadRightThumbstickUp: return GamepadKey.RightThumbstickUp;
                case VirtualKey.GamepadRightThumbstickDown: return GamepadKey.RightThumbstickDown;
                case VirtualKey.GamepadRightThumbstickRight: return GamepadKey.RightThumbstickRight;
                case VirtualKey.GamepadRightThumbstickLeft: return GamepadKey.RightThumbstickLeft;
                default: return GamepadKey.Other;
            }
        }

        static public KeyboardKey MapKeyboardKey(VirtualKey key) {
            switch (key) {
                case VirtualKey.Cancel: return KeyboardKey.Cancel;
                case VirtualKey.Back: return KeyboardKey.Back;
                case VirtualKey.Tab: return KeyboardKey.Tab;
                case VirtualKey.Clear: return KeyboardKey.Clear;
                case VirtualKey.Enter: return KeyboardKey.Enter;
                case VirtualKey.Shift: return KeyboardKey.Shift;
                case VirtualKey.Control: return KeyboardKey.Control;
                case VirtualKey.Menu: return KeyboardKey.Menu;
                case VirtualKey.Pause: return KeyboardKey.Pause;
                case VirtualKey.CapitalLock: return KeyboardKey.CapitalLock;
                case VirtualKey.Kana: return KeyboardKey.Kana_Hangul;
                case VirtualKey.Junja: return KeyboardKey.Junja;
                case VirtualKey.Final: return KeyboardKey.Final;
                case VirtualKey.Hanja: return KeyboardKey.Hanja_Kanji;
                case VirtualKey.Escape: return KeyboardKey.Escape;
                case VirtualKey.Convert: return KeyboardKey.Convert;
                case VirtualKey.NonConvert: return KeyboardKey.NonConvert;
                case VirtualKey.Accept: return KeyboardKey.Accept;
                case VirtualKey.ModeChange: return KeyboardKey.ModeChange;
                case VirtualKey.Space: return KeyboardKey.Space;
                case VirtualKey.PageUp: return KeyboardKey.PageUp;
                case VirtualKey.PageDown: return KeyboardKey.PageDown;
                case VirtualKey.End: return KeyboardKey.End;
                case VirtualKey.Home: return KeyboardKey.Home;
                case VirtualKey.Left: return KeyboardKey.Left;
                case VirtualKey.Up: return KeyboardKey.Up;
                case VirtualKey.Right: return KeyboardKey.Right;
                case VirtualKey.Down: return KeyboardKey.Down;
                case VirtualKey.Select: return KeyboardKey.Select;
                case VirtualKey.Print: return KeyboardKey.Print;
                case VirtualKey.Execute: return KeyboardKey.Execute;
                case VirtualKey.Snapshot: return KeyboardKey.Snapshot;
                case VirtualKey.Insert: return KeyboardKey.Insert;
                case VirtualKey.Delete: return KeyboardKey.Delete;
                case VirtualKey.Help: return KeyboardKey.Help;
                case VirtualKey.Number0: return KeyboardKey.Number0;
                case VirtualKey.Number1: return KeyboardKey.Number1;
                case VirtualKey.Number2: return KeyboardKey.Number2;
                case VirtualKey.Number3: return KeyboardKey.Number3;
                case VirtualKey.Number4: return KeyboardKey.Number4;
                case VirtualKey.Number5: return KeyboardKey.Number5;
                case VirtualKey.Number6: return KeyboardKey.Number6;
                case VirtualKey.Number7: return KeyboardKey.Number7;
                case VirtualKey.Number8: return KeyboardKey.Number8;
                case VirtualKey.Number9: return KeyboardKey.Number9;
                case VirtualKey.A: return KeyboardKey.A;
                case VirtualKey.B: return KeyboardKey.B;
                case VirtualKey.C: return KeyboardKey.C;
                case VirtualKey.D: return KeyboardKey.D;
                case VirtualKey.E: return KeyboardKey.E;
                case VirtualKey.F: return KeyboardKey.F;
                case VirtualKey.G: return KeyboardKey.G;
                case VirtualKey.H: return KeyboardKey.H;
                case VirtualKey.I: return KeyboardKey.I;
                case VirtualKey.J: return KeyboardKey.J;
                case VirtualKey.K: return KeyboardKey.K;
                case VirtualKey.L: return KeyboardKey.L;
                case VirtualKey.M: return KeyboardKey.M;
                case VirtualKey.N: return KeyboardKey.N;
                case VirtualKey.O: return KeyboardKey.O;
                case VirtualKey.P: return KeyboardKey.P;
                case VirtualKey.Q: return KeyboardKey.Q;
                case VirtualKey.R: return KeyboardKey.R;
                case VirtualKey.S: return KeyboardKey.S;
                case VirtualKey.T: return KeyboardKey.T;
                case VirtualKey.U: return KeyboardKey.U;
                case VirtualKey.V: return KeyboardKey.V;
                case VirtualKey.W: return KeyboardKey.W;
                case VirtualKey.X: return KeyboardKey.X;
                case VirtualKey.Y: return KeyboardKey.Y;
                case VirtualKey.Z: return KeyboardKey.Z;
                case VirtualKey.LeftWindows: return KeyboardKey.LeftWindows;
                case VirtualKey.RightWindows: return KeyboardKey.RightWindows;
                case VirtualKey.Application: return KeyboardKey.Application;
                case VirtualKey.Sleep: return KeyboardKey.Sleep;
                case VirtualKey.NumberPad0: return KeyboardKey.NumberPad0;
                case VirtualKey.NumberPad1: return KeyboardKey.NumberPad1;
                case VirtualKey.NumberPad2: return KeyboardKey.NumberPad2;
                case VirtualKey.NumberPad3: return KeyboardKey.NumberPad3;
                case VirtualKey.NumberPad4: return KeyboardKey.NumberPad4;
                case VirtualKey.NumberPad5: return KeyboardKey.NumberPad5;
                case VirtualKey.NumberPad6: return KeyboardKey.NumberPad6;
                case VirtualKey.NumberPad7: return KeyboardKey.NumberPad7;
                case VirtualKey.NumberPad8: return KeyboardKey.NumberPad8;
                case VirtualKey.NumberPad9: return KeyboardKey.NumberPad9;
                case VirtualKey.Multiply: return KeyboardKey.Multiply;
                case VirtualKey.Add: return KeyboardKey.Add;
                case VirtualKey.Separator: return KeyboardKey.Separator;
                case VirtualKey.Subtract: return KeyboardKey.Subtract;
                case VirtualKey.Decimal: return KeyboardKey.Decimal;
                case VirtualKey.Divide: return KeyboardKey.Divide;
                case VirtualKey.F1: return KeyboardKey.F1;
                case VirtualKey.F2: return KeyboardKey.F2;
                case VirtualKey.F3: return KeyboardKey.F3;
                case VirtualKey.F4: return KeyboardKey.F4;
                case VirtualKey.F5: return KeyboardKey.F5;
                case VirtualKey.F6: return KeyboardKey.F6;
                case VirtualKey.F7: return KeyboardKey.F7;
                case VirtualKey.F8: return KeyboardKey.F8;
                case VirtualKey.F9: return KeyboardKey.F9;
                case VirtualKey.F10: return KeyboardKey.F10;
                case VirtualKey.F11: return KeyboardKey.F11;
                case VirtualKey.F12: return KeyboardKey.F12;
                case VirtualKey.F13: return KeyboardKey.F13;
                case VirtualKey.F14: return KeyboardKey.F14;
                case VirtualKey.F15: return KeyboardKey.F15;
                case VirtualKey.F16: return KeyboardKey.F16;
                case VirtualKey.F17: return KeyboardKey.F17;
                case VirtualKey.F18: return KeyboardKey.F18;
                case VirtualKey.F19: return KeyboardKey.F19;
                case VirtualKey.F20: return KeyboardKey.F20;
                case VirtualKey.F21: return KeyboardKey.F21;
                case VirtualKey.F22: return KeyboardKey.F22;
                case VirtualKey.F23: return KeyboardKey.F23;
                case VirtualKey.F24: return KeyboardKey.F24;
                case VirtualKey.NavigationView: return KeyboardKey.NavigationView;
                case VirtualKey.NavigationMenu: return KeyboardKey.NavigationMenu;
                case VirtualKey.NavigationUp: return KeyboardKey.NavigationUp;
                case VirtualKey.NavigationDown: return KeyboardKey.NavigationDown;
                case VirtualKey.NavigationLeft: return KeyboardKey.NavigationLeft;
                case VirtualKey.NavigationRight: return KeyboardKey.NavigationRight;
                case VirtualKey.NavigationAccept: return KeyboardKey.NavigationAccept;
                case VirtualKey.NavigationCancel: return KeyboardKey.NavigationCancel;
                case VirtualKey.NumberKeyLock: return KeyboardKey.NumberKeyLock;
                case VirtualKey.Scroll: return KeyboardKey.Scroll;
                case VirtualKey.LeftShift: return KeyboardKey.LeftShift;
                case VirtualKey.RightShift: return KeyboardKey.RightShift;
                case VirtualKey.LeftControl: return KeyboardKey.LeftControl;
                case VirtualKey.RightControl: return KeyboardKey.RightControl;
                case VirtualKey.LeftMenu: return KeyboardKey.LeftMenu;
                case VirtualKey.RightMenu: return KeyboardKey.RightMenu;
                case VirtualKey.GoBack: return KeyboardKey.GoBack;
                case VirtualKey.GoForward: return KeyboardKey.GoForward;
                case VirtualKey.Refresh: return KeyboardKey.Refresh;
                case VirtualKey.Stop: return KeyboardKey.Stop;
                case VirtualKey.Search: return KeyboardKey.Search;
                case VirtualKey.Favorites: return KeyboardKey.Favorites;
                case VirtualKey.GoHome: return KeyboardKey.GoHome;
                default: return KeyboardKey.Other;
            }
        }
    }
}

