﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Numerics;

namespace LensflareAIContest {
    public class AIWil2 : AI {
        public override void TimeElapsed(float secondsSinceLastTick, float secondsTotal) {
        }

        public override Vector2 ThrustForUnit(int i) {
            var myHeart = Info.HeartInfoForTeamIndex(TeamIndex);
            var enemyHeart = Info.HeartInfoForTeamIndex(EnemyTeamIndex);
            Debug.Assert(myHeart != null && enemyHeart != null);
            Vector2 myHeartPos = myHeart.Position;
            Vector2 enemyHeartPos = enemyHeart.Position;
            var myUnits = Info.UnitsInfoForTeamIndex(TeamIndex);
            var enemyUnits = Info.UnitsInfoForTeamIndex(EnemyTeamIndex);
            Debug.Assert(myUnits != null && enemyUnits != null);

            var unit = myUnits[i];

            if (unit.Energy < 0.3f) {
                if (i < myUnits.Length / 2) {
                    Vector2 diff = unit.Position - myHeartPos;
                    double a = Math.Atan2(diff.Y, diff.X);
                    a += Math.PI * 0.90; //0.98f;
                    Vector2 thrust = new Vector2((float)Math.Cos(a), (float)Math.Sin(a));
                    return thrust;

                    //} else if (i < myUnits.Length * 2 / 3) {

                } else {
                    Vector2 diff = enemyHeartPos - unit.Position;
                    double a = Math.Atan2(diff.Y, diff.X);
                    a += Math.PI + Math.PI * 0.90; //0.98f;
                    Vector2 thrust = new Vector2((float)Math.Cos(a), (float)Math.Sin(a));
                    return thrust;
                }
            } else {
                return -unit.Velocity * 100;
            }
        }

        public override Vector2? PreparedShotForUnit(int i) {
            var myUnits = Info.UnitsInfoForTeamIndex(TeamIndex);
            var enemyUnits = Info.UnitsInfoForTeamIndex(EnemyTeamIndex);
            var enemyHeart = Info.HeartInfoForTeamIndex(EnemyTeamIndex);
            Debug.Assert(myUnits != null && enemyUnits != null);

            bool enemyHasUnits = enemyUnits.Length > 0;

            Vector2 posOfMyCurrentUnit = myUnits[i].Position;
            Vector2 velOfMyCurrentUnit = myUnits[i].Velocity;

            int indexOfUnitWithMinDist = ClosestEnemyUnitIndex(posOfMyCurrentUnit);

            Vector2 targetPos = Vector2.Zero;
            if (indexOfUnitWithMinDist == -1) {
                targetPos = enemyHeart.Position;
            } else if (indexOfUnitWithMinDist >= 0) {
                targetPos = enemyUnits[indexOfUnitWithMinDist].Position;
            }

            Vector2 diff = targetPos - posOfMyCurrentUnit;
            if (velOfMyCurrentUnit.Length() < 0.1 + (new Random()).NextDouble()*0.05f) {
                return diff;
            } else {
                return null;
            }
        }

        int ClosestEnemyUnitIndex(Vector2 pos) {
            var enemyUnits = Info.UnitsInfoForTeamIndex(EnemyTeamIndex);
            var enemyHeart = Info.HeartInfoForTeamIndex(EnemyTeamIndex);
            Debug.Assert(enemyUnits != null);

            float minDistToEnemyUnit = float.PositiveInfinity;
            int indexOfUnitWithMinDist = -2;

            for (int enemyUnitIndex = -1; enemyUnitIndex < enemyUnits.Length; ++enemyUnitIndex) {
                float dist = float.PositiveInfinity;
                if (enemyUnitIndex == -1) {
                    dist = (pos - enemyHeart.Position).LengthSquared();
                } else {
                    UnitInfo enemyUnit = enemyUnits[enemyUnitIndex];
                    dist = (pos - enemyUnit.Position).LengthSquared();
                }
                if (dist < minDistToEnemyUnit) {
                    minDistToEnemyUnit = dist;
                    indexOfUnitWithMinDist = enemyUnitIndex;
                }
            }

            return indexOfUnitWithMinDist;
        }

        bool EqualVelocity(Vector2 v1, Vector2 v2) {
            Random random = new Random();
            float epsilon = 0.1f + (float)random.NextDouble() * 0.01f;
            Vector2 d = v1 - v2;
            return Math.Abs(d.X) < epsilon && Math.Abs(d.Y) < epsilon;
        }

        public override String GetVersion() {
            return "Test Wil 1.0.1.2";
        }
    }
}
