﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Windows.UI;

namespace Util {
    public static class ColorExtension {
        /*
        public static Color Sum(this Color color0, Color color1) {
            return new Color(color0.ToVector4() + color1.ToVector4());
        }

        public static Color Mix(this Color color0, Color color1, float mix) {
            return Sum(color0 * (1.0f - mix), color1 * mix);
        }
        */

        public static Color Sum(this Color color0, Color color1) {
            return Color.FromArgb(
                (byte)(color0.A + color1.A),
                (byte)(color0.R + color1.R),
                (byte)(color0.G + color1.G),
                (byte)(color0.B + color1.B)
            );
        }

        public static Color Mix(this Color color0, Color color1, float mix) {
            var invertedMix = 1.0f - mix;
            return Color.FromArgb(
                (byte)(color0.A * invertedMix + color1.A * mix),
                (byte)(color0.R * invertedMix + color1.R * mix),
                (byte)(color0.G * invertedMix + color1.G * mix),
                (byte)(color0.B * invertedMix + color1.B * mix)
            );
        }
    }
}
