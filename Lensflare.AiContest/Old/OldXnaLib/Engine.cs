﻿/*
using System;
using System.Numerics;
using Util;

namespace LensflareGameFramework {
    public class Engine {        
        Random random = new Random(); //TODO: needed?
        Vector3 debugVector = Vector3.Zero;

        public Game Game { get; protected set; }
        public GraphicsDeviceManager GraphicsDeviceManager { get; protected set; }

        public float ambientPower = 0.2f;
        public float lightPower = 1.0f;
        public Vector3 lightDirection = new Vector3(1.0f, -1.0f, 1.0f);
        public Vector3 lightPos = new Vector3(0, 10.0f, 0);
        public Matrix lightsViewProjectionMatrix;

        protected float timeSinceLastFpsUpdate;
        protected int framesSinceLastFpsUpdate;
        public float Fps { get; protected set; }
        public float FpsUpdateDelay { get; set; }

        public bool MouseCursorCentering { get; set; }

        public Viewport Viewport { get { return Game.GraphicsDevice.Viewport; } }

        public Engine(Game game) {
            Game = game;
            FpsUpdateDelay = 0.5f;

            GraphicsDeviceManager = new GraphicsDeviceManager(Game);
        }

        public void Initialize() {
            Input.Update();
        }

        public void Load() {
            Primitive2.Init(Game.GraphicsDevice);

            RasterizerState rasterizerState = new RasterizerState();
            rasterizerState.CullMode = CullMode.CullCounterClockwiseFace;
            rasterizerState.MultiSampleAntiAlias = true;
            Game.GraphicsDevice.RasterizerState = rasterizerState;

            CenterMouseCursor();
        }

        public void Update2D(GameTime gameTime) {
            Update2D(gameTime, true, true);
        }

        public void Update2D(GameTime gameTime, bool entities, bool input) {
            if (entities) {
                Entity.UpdateAll(gameTime);
            }
            if (MouseCursorCentering) {
                CenterMouseCursor();
            }
            if (input) {
                Input.Update();
            }
        }

        public void Draw2D(GameTime gameTime) {
            float elapsedSeconds = (float)gameTime.ElapsedGameTime.TotalSeconds;
            timeSinceLastFpsUpdate += elapsedSeconds;
            framesSinceLastFpsUpdate += 1;
            if (timeSinceLastFpsUpdate >= FpsUpdateDelay) {
                timeSinceLastFpsUpdate -= FpsUpdateDelay;
                Fps = (int)(framesSinceLastFpsUpdate / FpsUpdateDelay);
                framesSinceLastFpsUpdate = 0;
            }
        }

        protected void DrawScene(String technique) {
            Entity.DrawAll();
        }

        protected void CenterMouseCursor() {
            Viewport vp = Viewport;
            Mouse.SetPosition(vp.Width / 2, vp.Height / 2);
        }
    }
}
*/