﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Graphics.Canvas;

namespace Lensflare.AiContest.Old.Facade {
    public class SpriteBatch {
        public CanvasDrawingSession DrawingSession;

        public SpriteBatch(CanvasDrawingSession drawingSession) {
            DrawingSession = drawingSession;
        }
    }
}
