﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lensflare.AiContest.Old.Facade {
    public class GameTime {
        public class ElapsedGameTimeFacade {
            public double TotalSeconds = 1.0 / 60.0;
        }

        public ElapsedGameTimeFacade ElapsedGameTime = new ElapsedGameTimeFacade();
    }
}
