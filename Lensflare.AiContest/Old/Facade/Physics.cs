﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Lensflare.AiContest.Old.Facade {
    public class Shape {
    }

    public class CircleShape : Shape {
    }

    public class Body {
        public Vector2 Position = Vector2.Zero;
        public Vector2 LinearVelocity = Vector2.Zero;
    }   
}
