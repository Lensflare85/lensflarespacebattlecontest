﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;

namespace LensflareAIContest {
    public class SpaceshipRules {
        public float AccelerationMax { get; internal set; }
        public float HealthMax { get; internal set; }
        public float EnergyMax { get; internal set; }
        public float EnergyNeededForShot { get; internal set; }
        public float EnergyRegenerationRate { get; internal set; }
        public float Radius { get; internal set; }
        public float CountPerTeam { get; internal set; }

        public SpaceshipRules Clone() {
            return (SpaceshipRules)MemberwiseClone();
        }
    }

    public class HeartRules {
        public float HealthMax { get; internal set; }
        public float Radius { get; internal set; }
        public float OffsetFromWorldCenter { get; internal set; }

        public HeartRules Clone() {
            return (HeartRules)MemberwiseClone();
        }
    }

    public class ProjectileRules {
        public float VelocityInitial { get; internal set; }
        public float Radius { get; internal set; }
        public float LifeSpan { get; internal set; }
        public float DamageCausedOnHit { get; internal set; }

        public ProjectileRules Clone() {
            return (ProjectileRules)MemberwiseClone();
        }
    }

    public class WorldRules {
        public int TeamCount { get; internal set; }
        public float PhysicsSpaceScale { get; internal set; }
        public Vector2 Extent { get; internal set; }
        public float UnitSpawnDistanceFromHeart { get; internal set; }

        public WorldRules Clone() {
            return (WorldRules)MemberwiseClone();
        }
    }

    public class Rules {
        public SpaceshipRules Spaceship { get; internal set; }
        public HeartRules Heart { get; internal set; }
        public ProjectileRules Projectile { get; internal set; }
        public WorldRules World { get; internal set; }

        public Rules Clone() {
            return new Rules() {
                Spaceship = Spaceship.Clone(),
                Heart = Heart.Clone(),
                Projectile = Projectile.Clone(),
                World = World.Clone(),
            };
        }
    }
}
