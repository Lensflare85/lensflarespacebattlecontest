﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LensflareGameFramework;

namespace LensflareAIContest {
    abstract public class GameEntity : Entity {
        public LensflareAIContestGame Game { get; protected set; }

        public GameEntity(LensflareAIContestGame game) {
            Game = game;
        }

        protected override void Dispose() {
            //Game = null;
        }
    }
}
