﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Util;
using System.Numerics;
using Windows.UI;
using Lensflare.AiContest.Old.Facade;

namespace LensflareAIContest {
    class WorldFrameEntity : GameEntity {
        Vector2 size;
        Body body;

        public WorldFrameEntity(LensflareAIContestGame game, Vector2 position, Vector2 size) : base(game) {
            this.size = size;

            float wHalf = this.size.X * 0.5f * Game.Rules.World.PhysicsSpaceScale;
            float hHalf = this.size.Y * 0.5f * Game.Rules.World.PhysicsSpaceScale;

            body = new Body();
            /*
            body = new Body(game.World);
            body.BodyType = BodyType.Static;
            
            EdgeShape leftShape =  new EdgeShape(new Vector2(0 - wHalf, 0 - hHalf), new Vector2(0 - wHalf, 0 + hHalf));
            EdgeShape rightShape = new EdgeShape(new Vector2(0 + wHalf, 0 - hHalf), new Vector2(0 + wHalf, 0 + hHalf));
            EdgeShape topShape =   new EdgeShape(new Vector2(0 - wHalf, 0 - hHalf), new Vector2(0 + wHalf, 0 - hHalf));
            EdgeShape downShape =  new EdgeShape(new Vector2(0 - wHalf, 0 + hHalf), new Vector2(0 + wHalf, 0 + hHalf));

            new Fixture(body, leftShape);
            new Fixture(body, rightShape);
            new Fixture(body, topShape);
            new Fixture(body, downShape);
            */
        }

        protected override void Dispose() {
            //body.Dispose();
            base.Dispose();
        }

        public override void Update(GameTime gameTime) {

        }

        public override void Draw() {
            Vector2 screenPosition = Game.Camera.Project(Vector2.Zero);
            Vector2 zoomedSize = size * Game.Camera.Zoom;
            Vector2 position = body.Position / Game.Rules.World.PhysicsSpaceScale;

            var borderColor = Color.FromArgb(255, 70, 0, 50);
            Primitive2.DrawRect(Game.SpriteBatch, screenPosition + position - zoomedSize * 0.5f, zoomedSize, borderColor, false, Game.LayerManager.Depth((int)MainLayer.Wall));
        }
    }
}
