﻿using System.Numerics;
using Lensflare.AiContest.Old.Facade;

namespace LensflareAIContest {
    public class Heart : GameEntity {
        private Body body;
		private CircleShape shape;
		private float health;

		private Bar healthBar;

		private bool showHealthBar = true;

        public int TeamIndex { get; protected set; }

        public static Texture2D Texture { get; set; }

        Vector2 position; //temp

        public Heart(LensflareAIContestGame game, Vector2 position, int teamIndex) : base(game) {
            TeamIndex = teamIndex;

            health = Game.Rules.Heart.HealthMax;
			healthBar = new Bar( game,  1.0f );

            Texture = new Texture2D();
            body = new Body();
            /*
            body = new Body(game.World);
            body.BodyType = BodyType.Static;
            body.SetTransform(position * Game.Rules.World.PhysicsSpaceScale, 0);
            body.UserData = this;

            shape = new CircleShape(Game.Rules.Heart.Radius * Game.Rules.World.PhysicsSpaceScale, 1.0f);

            new Fixture(body, shape);
            */

            this.position = position; //temp
        }

        protected override void Dispose() {
            /*
            body.UserData = null;
            body.Dispose();
            */
            base.Dispose();
        }

        public Vector2 Position {
            get {
                //return body.Position / Game.Rules.World.PhysicsSpaceScale;
                return position; //temp
            }
        }

        public float Health { get { return health; } }

		public bool ShowHealthBar { get { return this.showHealthBar; } set { this.showHealthBar = value; } }

        public override void Update(GameTime gameTime) {
            
        }

        public void ApplyDamage(float damage) {
            float healthBeforeDamage = health;
            health -= damage;
            healthBar.Value = (health / Game.Rules.Heart.HealthMax);

            Game.Events.AddLast(new HeartEvent(HeartEvent.EventReason.Damaged, this.TeamIndex));

            if (health <= 0.0f) {
                bool destroyedRightNow = healthBeforeDamage > 0.0f;
                if (destroyedRightNow) {
                    Game.Events.AddLast(new HeartEvent(HeartEvent.EventReason.Destroyed, this.TeamIndex));
                }

                //declare the other team as the winner:
                if (Game.WinnerIndex < 0) {
                    Game.WinnerIndex = this.TeamIndex == 0 ? 1 : 0;
                }
            }
        }

        public override void Draw() {
            if (Game.Camera.OverlapsWorldShape(shape, body, Game.Rules.World.PhysicsSpaceScale)) {

                Vector2 screenPosition = Game.Camera.Project(Position);
                float zoomedRadius = Game.Rules.Heart.Radius * Game.Camera.Zoom;

                bool alive = Health > 0.0f;

				#region HealthBar

				if ( ShowHealthBar )
				{
                    float a = 0.7f;

					float healthBarDistanceFromHeart = 2.0f;
					Vector2 healthBarSize = new Vector2( zoomedRadius * 2, zoomedRadius / 2 );
					Vector2 healthBarPosition = new Vector2( screenPosition.X - zoomedRadius,
															 screenPosition.Y - zoomedRadius - healthBarSize.Y - healthBarDistanceFromHeart );

                    healthBar.BorderColor = Game.PlayerColors[TeamIndex]; //Game.PlayerColors[TeamIndex] * a;
                    healthBar.FillColor = Game.PlayerColors[TeamIndex]; //Game.PlayerColors[TeamIndex] * a;

                    healthBar.Position = healthBarPosition;
					healthBar.Size = healthBarSize;
					healthBar.Draw();
				}

				#endregion

                /*
                Primitive2.DrawCircle(Game.SpriteBatch, screenPosition, zoomedRadius - 2, c1, alive, Game.LayerManager.Depth((int)MainLayer.DynamicEntity));
                Primitive2.DrawCircle(Game.SpriteBatch, screenPosition, zoomedRadius - 1, c2, false, Game.LayerManager.Depth((int)MainLayer.DynamicEntity));
                Primitive2.DrawCircle(Game.SpriteBatch, screenPosition, zoomedRadius, c1, false, Game.LayerManager.Depth((int)MainLayer.DynamicEntity));
                */

                float layerDepth = Game.LayerManager.Depth((int)MainLayer.DynamicEntity);
                float textureZoom = Game.Rules.Heart.Radius * 2.0f / Texture.Width;
                //Game.SpriteBatch.Draw(Texture, screenPosition, null, Game.PlayerColors[TeamIndex], 0, Texture.GetCenter(), Game.Camera.Zoom * textureZoom, SpriteEffects.None, layerDepth);
                Game.SpriteBatch.DrawingSession.DrawCircle(screenPosition, zoomedRadius, Game.PlayerColors[TeamIndex]);
            }
        }
    }
}
