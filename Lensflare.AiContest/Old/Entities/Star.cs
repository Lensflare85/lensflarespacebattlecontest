﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Util;
using Camera;
using Windows.UI;
using System.Numerics;
using Lensflare.AiContest.Old.Facade;

namespace LensflareAIContest {
    class Star : GameEntity {
        protected Texture2D texture;
        protected Color color;

        protected float radius;
        protected float parallax;
        protected float scale;

        public Vector2 Position { get; protected set; }

        public Star(LensflareAIContestGame game, Texture2D texture) : base(game) {
            this.texture = texture;

            radius = (1.0f + (float)Game.Random.NextDouble()) * 5.0f;
            parallax = (20.0f + radius) * 0.01f;

            //float z = (float)Math.Pow(Game.Camera.Zoom, parallax);
            scale = radius * 2.0f /* *z */ / texture.Height * 0.3f;

            this.Position = new Vector2((((float)Game.Random.NextDouble()) * 2.0f - 1.0f) * 1200.0f, (((float)Game.Random.NextDouble()) * 2.0f - 1.0f) * 700.0f) * 1.5f;

            const float cStart = 0.85f;
            const float cVar = 1.0f - cStart;
            float cR = cStart + (float)Game.Random.NextDouble() * cVar;
            float cG = cStart + (float)Game.Random.NextDouble() * cVar;
            float cB = cStart + (float)Game.Random.NextDouble() * cVar;
            float alpha = (radius - 4.0f) / 6.0f * 0.5f;

            //color = new Color(cR, cG, cB) * alpha;
            color = Color.FromArgb((byte)(alpha * 255), (byte)(cR * 255), (byte)(cG * 255), (byte)(cB * 255));
        }

        public override void Update(GameTime gameTime) {

        }

        public override void Draw() {
            Camera2 camera = Game.Camera;
            
            Vector2 screenPosition = camera.Project(Position, parallax);
            if (camera.OverlapsRect(screenPosition - Vector2.One * radius, Vector2.One * radius * 2)) {
                //Game.SpriteBatch.Draw(texture, screenPosition, null, color, 0, texture.GetCenter(), scale, SpriteEffects.None, Game.LayerManager.Depth((int)MainLayer.Star));
            }
        }
    }
}
