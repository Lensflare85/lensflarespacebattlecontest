﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Util;
using System.Numerics;
using Windows.UI;
using Lensflare.AiContest.Old.Facade;

namespace LensflareAIContest
{
	public class Bar : GameEntity
	{
		private Vector2 position;
		private Vector2 size;
		private float value;

		private Vector2 valuePosition;
		private Vector2 valueSize;

        //public Texture2D FillTexture;

		public Bar(LensflareAIContestGame game, float startValue) : base(game)
		{
			this.BorderSize = 0.5f;
			this.BorderColor = Color.FromArgb(255,0,0,0);
			this.FillColor = Color.FromArgb(255, 255, 255, 255);
			this.Value = startValue;
		}

		#region Properties

		public float BorderSize { get; set; }
		public Color BorderColor { get; set; }
		public Color FillColor { get; set; }

		public Vector2 Position
		{
			get { return this.position; }
			set
			{
				this.position = value;
				UpdateValueBarPosition();
			}
		}
		public Vector2 Size
		{
			get { return this.size; }
			set
			{
				this.size = value;
				UpdateValueBarSize();
			}
		}
		public float Value
		{
			get { return this.value; }
			set
			{ 
				this.value = value;
				UpdateValueBarSize();
			}
		}		

		#endregion

		private void UpdateValueBarPosition()
		{
			this.valuePosition = new Vector2( this.position.X + this.BorderSize,
											  this.position.Y + this.BorderSize );
			
		}
		private void UpdateValueBarSize()
		{
			this.valueSize = new Vector2( ( this.size.X - 2 * this.BorderSize ) * this.value,
											this.size.Y - 2 * this.BorderSize );
		}

		public override void Update( GameTime gameTime )
		{
			//nothing to do
		}

		public override void Draw()
		{
            float layerDepth = Game.LayerManager.Depth( (int)MainLayer.DynamicEntity );

            Primitive2.DrawRect(Game.SpriteBatch, this.position, this.size, this.BorderColor, false, layerDepth);
			Primitive2.DrawRect( Game.SpriteBatch, this.valuePosition, this.valueSize, this.FillColor, true, Game.LayerManager.Depth( (int)MainLayer.DynamicEntity ) );

            //Game.SpriteBatch.Draw(FillTexture, position, null, Color.White, 0, Vector2.Zero, new Vector2(size.X*value, size.Y), SpriteEffects.None, layerDepth);
		}
	}
}
