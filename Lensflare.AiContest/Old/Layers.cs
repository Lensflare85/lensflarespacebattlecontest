﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LensflareAIContest {
    public enum MainLayer {
        MouseCursor,
        Hud,
        Wall,
        DynamicEntity,
        Star,
        Ground,
        Background,
    }

    public enum HudLayer {
        Text,
        TextShadow,
        Panel,
    }
}
