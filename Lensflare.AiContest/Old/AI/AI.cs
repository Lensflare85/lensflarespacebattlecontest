﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;

namespace LensflareAIContest {
    abstract public class AI {
        public Info Info { get; set; }
        public int TeamIndex { get; set; }
        public int EnemyTeamIndex { get { return TeamIndex == 0 ? 1 : 0; } }

        abstract public void TimeElapsed(float secondsSinceLastTick, float secondsTotal);
        abstract public Vector2 ThrustForUnit(int index);
        abstract public Vector2? PreparedShotForUnit(int index);
        abstract public String GetVersion();
    }
}
