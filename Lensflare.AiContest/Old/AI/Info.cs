﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Util;
using System.Numerics;

namespace LensflareAIContest {
    public class UnitInfo {
        public int ID { get; protected set; }
        public Vector2 Position { get; protected set; }
        public Vector2 Velocity { get; protected set; }
        public Vector2 Thrust { get; protected set; }
        public float Health { get; protected set; }
        public float Energy { get; protected set; }
        public int TeamIndex { get; protected set; }

        public UnitInfo(int id, int teamIndex, float health, float energy, Vector2 position, Vector2 velocity, Vector2 thrust) {
            this.ID = id;
            this.TeamIndex = teamIndex;
            this.Health = health;
            this.Energy = energy;
            this.Position = position;
            this.Velocity = velocity;
            this.Thrust = thrust;
        }
    }

    public class HeartInfo {
        public Vector2 Position { get; protected set; }
        public float Health { get; protected set; }

        public HeartInfo(float health, Vector2 position) {
            this.Health = health;
            this.Position = position;
        }
    }

    public class Info {
        protected LensflareAIContestGame Game { get; set; }
        public Rules Rules { get; protected set; }

        public Info(LensflareAIContestGame game) {
            this.Game = game;
            Rules = Game.Rules.Clone();
        }

        public Event[] Events {
            get {
                int n = Game.Events.Count;
                Event[] events = new Event[n];
                int i = 0;
                foreach (Event e in Game.Events) {
                    events[i++] = e.Clone();
                }
                return events;
            }
        }

        public UnitInfo[] UnitsInfoForTeamIndex(int teamIndex) {
            if (teamIndex >= 0 && teamIndex < Game.Teams.Length) {
                var units = Game.Teams[teamIndex];
                UnitInfo[] info = new UnitInfo[units.Count];
                for (int i = 0; i < units.Count; ++i) {
                    Spaceship unit = units[i];
                    info[i] = new UnitInfo(unit.ID, unit.TeamIndex, unit.Health, unit.Energy, unit.Position, unit.Velocity, unit.Thrust);
                }
                return info;
            } else {
                return null;
            }
        }

        public HeartInfo HeartInfoForTeamIndex(int teamIndex) {
            if (teamIndex >= 0 && teamIndex < Game.Teams.Length) {
                Heart heart = Game.Hearts[teamIndex];
                return new HeartInfo(heart.Health, heart.Position);
            } else {
                return null;
            }
        }
    }
}
