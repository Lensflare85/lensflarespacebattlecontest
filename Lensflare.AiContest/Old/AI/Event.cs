﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LensflareAIContest {
    public class Event {
        //public float Time { get; protected set; }

        public Event() {

        }

        public Event Clone() {
            return (Event)this.MemberwiseClone();
        }
    }

    public class UnitEvent : Event {
        public enum EventReason {
            Destroyed,
            Damaged,
        };

        public EventReason Reason { get; protected set; }
        public UnitInfo Unit { get; protected set; }

        public UnitEvent(EventReason reason, UnitInfo unit) {
            Reason = reason;
            Unit = unit;
        }
    }

    public class HeartEvent : Event {
        public enum EventReason {
            Destroyed,
            Damaged,
        };

        public EventReason Reason { get; protected set; }
        public int TeamIndex { get; protected set; }

        public HeartEvent(EventReason reason, int teamIndex) {
            Reason = reason;
            TeamIndex = teamIndex;
        }
    }

    public class ProjectileEvent : Event {
        public enum EventReason {
            Launched,
            HitUnit,
            HitHeart,
            HitWall,
            Vanished,
        };

        public EventReason Reason { get; protected set; }

        //TODO: ...
        public ProjectileEvent(EventReason reason /* , ProjectileEntityInfo projectile */) {
            Reason = reason;
            //Projectile = projectile;
        }
    }
}
