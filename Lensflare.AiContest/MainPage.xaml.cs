﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices.WindowsRuntime;
using LensflareAIContest;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Lensflare.AiContest
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        LensflareAIContestGame game = new LensflareAIContestGame();

        public MainPage()
        {
            this.InitializeComponent();

            //temporary game loop with synchronized update/draw
            var timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(1);
            timer.Tick += (sender, args) => {
                game.Input.Update();
                
                game.Update(new Old.Facade.GameTime() {
                    ElapsedGameTime = new Old.Facade.GameTime.ElapsedGameTimeFacade() {  }
                });

                canvas.Invalidate(); //triggers canvas_Draw()
            };
            timer.Start();
        }

        private void canvas_Draw(Microsoft.Graphics.Canvas.UI.Xaml.CanvasControl sender, Microsoft.Graphics.Canvas.UI.Xaml.CanvasDrawEventArgs args) {
            args.DrawingSession.DrawText("Hello, World!", 100, 100, Colors.Blue);
            args.DrawingSession.DrawCircle(125, 125, 100, Colors.Green);
            args.DrawingSession.DrawLine(0, 0, 50, 200, Colors.Red);

            game.SpriteBatch = new Old.Facade.SpriteBatch(args.DrawingSession);
            game.Draw(new Old.Facade.GameTime() {
                ElapsedGameTime = new Old.Facade.GameTime.ElapsedGameTimeFacade() { }
            });
        }

        private void canvas_Unloaded(object sender, RoutedEventArgs e) {
            this.canvas.RemoveFromVisualTree();
            this.canvas = null;
        }

        private void canvas_CreateResources(Microsoft.Graphics.Canvas.UI.Xaml.CanvasControl sender, Microsoft.Graphics.Canvas.UI.CanvasCreateResourcesEventArgs args) {
            
        }

        private void canvas_Loaded(object sender, RoutedEventArgs e) {
            game.LoadContent();

            /*
            Window.Current.CoreWindow.KeyDown += (s, args) => {
                var input = (LensflareGameInterface.UwpWin2D.Input)game.Input;
                input.SetKey(args.VirtualKey, true);
            };
            Window.Current.CoreWindow.KeyUp += (s, args) => {
                var input = (LensflareGameInterface.UwpWin2D.Input)game.Input;
                input.SetKey(args.VirtualKey, false);
            };
            */
        }

        private void canvas_KeyDown(object sender, KeyRoutedEventArgs e) {

        }

        private void Page_KeyDown(object sender, KeyRoutedEventArgs e) {

        }
    }
}
